# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Javier Viñal <fjvinal@gmail.com>, 2013, 2014, 2015, 2016, 2017, 2018.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:47+0000\n"
"PO-Revision-Date: 2022-01-03 00:57+0100\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: sendlaterconfiguredialog.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr "Configurar"

#: sendlaterconfiguredialog.cpp:49
#, kde-format
msgid "Send Later Agent"
msgstr "Agente de enviar más tarde"

#: sendlaterconfiguredialog.cpp:51
#, kde-format
msgid "Send emails later agent."
msgstr "Agente de enviar mensajes más tarde."

#: sendlaterconfiguredialog.cpp:53
#, kde-format
msgid "Copyright (C) 2013-%1 Laurent Montel"
msgstr "Copyright © 2013-%1 Laurent Montel"

#: sendlaterconfiguredialog.cpp:55
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: sendlaterconfiguredialog.cpp:55
#, kde-format
msgid "Maintainer"
msgstr "Encargado"

#: sendlaterconfiguredialog.cpp:58
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Eloy Cuadra,Javier Viñal"

#: sendlaterconfiguredialog.cpp:58
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ecuadra@eloihr.net,fjvinal@gmail.com"

#: sendlaterconfigurewidget.cpp:55
#, kde-format
msgid "To"
msgstr "Para"

#: sendlaterconfigurewidget.cpp:55
#, kde-format
msgid "Subject"
msgstr "Asunto"

#: sendlaterconfigurewidget.cpp:55
#, kde-format
msgid "Send around"
msgstr "Enviar alrededor de"

#: sendlaterconfigurewidget.cpp:55
#, kde-format
msgid "Recurrent"
msgstr "Se repite"

#: sendlaterconfigurewidget.cpp:57
#, kde-format
msgid "Message Id"
msgstr "Id. de mensaje"

#: sendlaterconfigurewidget.cpp:68
#, kde-format
msgid "No messages waiting..."
msgstr "No hay mensajes esperando..."

#: sendlaterconfigurewidget.cpp:93
#, kde-format
msgid "Send now"
msgstr "Enviar ahora"

#. i18n: ectx: property (text), widget (QPushButton, deleteItem)
#: sendlaterconfigurewidget.cpp:96 ui/sendlaterconfigurewidget.ui:38
#, kde-format
msgid "Delete"
msgstr "Borrar"

#: sendlaterconfigurewidget.cpp:157
#, kde-format
msgid "Yes"
msgstr "Sí"

#: sendlaterconfigurewidget.cpp:157
#, kde-format
msgid "No"
msgstr "No"

#: sendlaterconfigurewidget.cpp:207
#, kde-format
msgid "Do you want to delete the selected item?"
msgid_plural "Do you want to delete the selected items?"
msgstr[0] "¿Quiere borrar el elemento seleccionado?"
msgstr[1] "¿Quiere borrar los elementos seleccionados?"

#: sendlaterconfigurewidget.cpp:208
#, kde-format
msgctxt "@title:window"
msgid "Delete Items"
msgstr "Borrar elementos"

#: sendlaterconfigurewidget.cpp:216
#, kde-format
msgid "Do you want to delete the message as well?"
msgid_plural "Do you want to delete the messages as well?"
msgstr[0] "¿Quiere borrar también el mensaje?"
msgstr[1] "¿Quiere borrar también los mensajes?"

#: sendlaterconfigurewidget.cpp:217
#, kde-format
msgctxt "@title:window"
msgid "Delete Messages"
msgstr "Borrar mensajes"

#: sendlaterconfigurewidget.cpp:219
#, kde-format
msgctxt "@action:button"
msgid "Do Not Delete"
msgstr "No borrar"

#: sendlaterjob.cpp:56 sendlaterjob.cpp:59
#, kde-format
msgid "Not message found."
msgstr "No se ha encontrado ningún mensaje."

#: sendlaterjob.cpp:67
#, kde-format
msgid "No message found."
msgstr "No se ha encontrado ningún mensaje."

#: sendlaterjob.cpp:76
#, kde-format
msgid "Error during fetching message."
msgstr "Se ha producido un error al recuperar un mensaje."

#: sendlaterjob.cpp:82
#, kde-format
msgid "Cannot fetch message. %1"
msgstr "No se ha podido obtener el mensaje. %1"

#: sendlaterjob.cpp:87
#, kde-format
msgid "We can't create transport"
msgstr "No podemos crear el transporte"

#: sendlaterjob.cpp:94
#, kde-format
msgid "Message is not a real message"
msgstr "El mensaje no es un mensaje real"

#: sendlaterjob.cpp:101
#, kde-format
msgid "Cannot send message."
msgstr "No se puede enviar el mensaje."

#: sendlaterjob.cpp:132
#, kde-format
msgid "Message sent"
msgstr "Mensaje enviado"

#: sendlatermanager.cpp:191
#, kde-format
msgid "An error was found. Do you want to resend it?"
msgstr "Se ha encontrado un error. ¿Quiere volver a enviarlo?"

#: sendlatermanager.cpp:192
#, kde-format
msgid "Error found"
msgstr "Se ha encontrado un error"

#: sendlatermanager.cpp:193
#, kde-format
msgctxt "@action:button"
msgid "Resend"
msgstr "Volver a enviar"

#. i18n: ectx: property (text), widget (QPushButton, modifyItem)
#: ui/sendlaterconfigurewidget.ui:31
#, kde-format
msgid "Modify..."
msgstr "Modificar..."

#~ msgid "Remove items"
#~ msgstr "Eliminar elementos"

#~ msgid "Remove"
#~ msgstr "Eliminar"
